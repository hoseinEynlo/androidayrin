package com.eynloo.ayrin.view.mainActivity;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.eynloo.ayrin.connection.ConnectionCheck;
import com.eynloo.ayrin.R;
import com.eynloo.ayrin.backTask.UpdateTask;
import com.eynloo.ayrin.view.internetCheck.InternetWarning;
import com.eynloo.ayrin.webService.Data.model.ResponseData;
import com.eynloo.ayrin.webService.postData.model.ResponsePostData;
import com.eynloo.ayrin.connection.WifiReciver;
import com.github.mikephil.charting.charts.LineChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.LineData;
import com.github.mikephil.charting.data.LineDataSet;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class V_mainActivity extends AppCompatActivity implements iV_mainActivity {


    private static final String THINGSPEAK_CHANNEL_ID = "1042764";
    private static final String THINGSPEAK_API_KEY_WRITE = "Y6YMU2VON3OHDA9E";
    private static final String THINGSPEAK_API_KEY_READ = "7R82VKJVOV2IVYO5";
    private static final String THINGSPEAK_FIELD1 = "ayrin";


    LineChart lineChart;
    LineData lineData;
    LineDataSet lineDataSet;
    ArrayList lineEntries;

    P_mainActivity p_mainActivity;


    EditText editTextInsertData;
    Button btnSubmit;
    private ResponseData responseData;

    Handler handler;
    Timer timer;
    TimerTask doAsynchronousTask;

    WifiReciver wifiReciver;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        init();


        if (!(ConnectionCheck.connectionState(this))) {

            startActivity(new Intent(this, InternetWarning.class));

        } else {


            p_mainActivity.sendRequestToGetData();
            callAsynchronousTask();

            btnSubmit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    setupEditText();

                }
            });

        }


    }


    @Override
    public Observer<List<String>> onSuccessGetData(final ResponseData responseData) {
        this.responseData = responseData;

        return new Observer<List<String>>() {
            @Override
            public void onSubscribe(Disposable d) {

            }

            @Override
            public void onNext(List<String> strings) {
                lineChart.clear();
                lineEntries.clear();
                lineChart.resetZoom();


                for (int i = 0; i < strings.size(); i++) {
                    lineEntries.add(new Entry(i, Integer.valueOf(strings.get(i))));
                }

                lineDataSet = new LineDataSet(lineEntries, "");
                lineData = new LineData(lineDataSet);
                lineChart.setData(lineData);
                lineDataSet.setColors(ColorTemplate.JOYFUL_COLORS);
                lineDataSet.setValueTextColor(Color.BLACK);


            }

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }
        };
    }

    @Override
    public void onFailedGetData(int errorId, String ErrorMessage) {

        Toast.makeText(this, "" + ErrorMessage, Toast.LENGTH_SHORT).show();
    }


    @Override
    public void onSuccessPostData(ResponsePostData responsePostData) {
        Toast.makeText(this, "داده با موفقیت ارسال شد", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onFailedPostData(int errorId, String ErrorMessage) {

        Toast.makeText(this, "خطا در ارسال داده!!", Toast.LENGTH_SHORT).show();
    }




    public void callAsynchronousTask() {
        handler = new Handler();
        timer = new Timer();
        doAsynchronousTask = new TimerTask() {
            @Override
            public void run() {
                handler.post(new Runnable() {
                    public void run() {
                        try {
                            UpdateTask performBackgroundTask = new UpdateTask(p_mainActivity);
                            // UpdateTask this class is the class that extends AsynchTask
                            performBackgroundTask.execute();
                        } catch (Exception e) {
                            // TODO Auto-generated catch block
                        }
                    }
                });
            }
        };
        timer.schedule(doAsynchronousTask, 0, 5000); //execute in every 50000 ms
    }


    private boolean isEmpty(EditText etText) {
        if (etText.getText().toString().trim().length() > 0)
            return false;

        return true;
    }


    private void init() {
        wifiReciver = new WifiReciver();
        lineEntries = new ArrayList<>();
        p_mainActivity = new P_mainActivity(this);
        lineChart = findViewById(R.id.lineChart);
        editTextInsertData = findViewById(R.id.editTextInsertData);
        btnSubmit = findViewById(R.id.btnSubmit);
    }


    private void setupEditText() {

        if (isEmpty(editTextInsertData)) {

            editTextInsertData.setError("is empty !!");


        } else {

            String txt = editTextInsertData.getText().toString();

            p_mainActivity.sendData(THINGSPEAK_API_KEY_WRITE, Integer.valueOf(txt));

        }
    }

    @Override
    public Context getContext() {
        return this;
    }

    @Override
    protected void onStart() {
        super.onStart();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(WifiManager.NETWORK_STATE_CHANGED_ACTION);
        registerReceiver(wifiReciver, intentFilter);
    }

    @Override
    protected void onStop() {
        super.onStop();
        unregisterReceiver(wifiReciver);

        if (timer != null) {
            timer.purge();
            timer.cancel();
        }
        if (doAsynchronousTask != null)
            doAsynchronousTask.cancel();

        V_mainActivity.this.finish();

    }
}


