package com.eynloo.ayrin.view.mainActivity;

import android.content.Context;

import com.eynloo.ayrin.webService.Data.model.ResponseData;
import com.eynloo.ayrin.webService.postData.model.ResponsePostData;

import java.util.List;

import io.reactivex.Observer;

public interface iV_mainActivity {

    //    void onSuccessGetData(ResponseData responseData);
    Observer<List<String>> onSuccessGetData(ResponseData responseData);

    void onFailedGetData(int errorId, String ErrorMessage);

    Context getContext();

    void onSuccessPostData(ResponsePostData responsePostData);

    void onFailedPostData(int errorId, String ErrorMessage);

}
