package com.eynloo.ayrin.view.splash;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.view.animation.AlphaAnimation;
import android.widget.ImageView;

import com.eynloo.ayrin.view.mainActivity.V_mainActivity;
import com.eynloo.ayrin.R;

public class SplashScreenActivity extends AppCompatActivity {

    private static final int Delay=4000;
    ImageView imageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);

        imageView=findViewById(R.id.image_splash);

        usesAnimation();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {

                startActivity(new Intent(SplashScreenActivity.this, V_mainActivity.class));
                SplashScreenActivity.this.finish();

            }
        },Delay);

    }

    private void usesAnimation() {

        AlphaAnimation alphaAnimation=new AlphaAnimation(0,1);
        alphaAnimation.setDuration(Delay);
        imageView.startAnimation(alphaAnimation);

    }

    @Override
    public void onBackPressed() {
    }
}
