package com.eynloo.ayrin.view.mainActivity;

import android.content.Context;

import com.eynloo.ayrin.webService.Data.model.Feed;
import com.eynloo.ayrin.webService.Data.model.ResponseData;
import com.eynloo.ayrin.webService.postData.model.ResponsePostData;

import java.util.List;

import io.reactivex.Observable;

public interface iP_mainActivity {



    Context getContext();

    void sendData(String api_key,int number);

    void onSuccessPostData(ResponsePostData responsePostData);

    void onFailedPostData(int errorId, String ErrorMessage);

    void sendRequestToGetData();

    void onSuccessGetData(ResponseData responseData,Observable<List<String>> observable);

    void onFailedGetData(int errorId, String ErrorMessage);


}
