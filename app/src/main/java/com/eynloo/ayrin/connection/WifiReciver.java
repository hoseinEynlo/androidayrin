package com.eynloo.ayrin.connection;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.NetworkInfo;
import android.net.wifi.WifiManager;

import com.eynloo.ayrin.view.internetCheck.InternetWarning;


public class WifiReciver extends BroadcastReceiver {


    @Override
    public void onReceive(Context context, Intent intent) {

        final String action = intent.getAction();

        if (action.equals(WifiManager.NETWORK_STATE_CHANGED_ACTION)) {
            NetworkInfo info = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
            boolean connected = info.isConnected();
            if (connected){

            }else {
                context.startActivity(new Intent(context, InternetWarning.class));
            }

        }

    }

}
